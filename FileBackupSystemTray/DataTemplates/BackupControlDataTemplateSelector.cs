﻿using FileBackupSystemTray.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace FileBackupSystemTray.DataTemplates
{
    public class BackupControlDataTemplateSelector: DataTemplateSelector
    {
        public DataTemplate InfoItemTemplate { get; set; }
        public DataTemplate BackupItemTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var infoTemplate = item as InfoItemViewModel;

            if (infoTemplate != null)
                return InfoItemTemplate;

            return BackupItemTemplate;
        }
    }
}
