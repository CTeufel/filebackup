﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace FileBackupSystemTray.Converters
{
    public class ToPercentageMultiValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null || values.Length != 2)
                return string.Empty;

            if (!int.TryParse(values[1].ToString(), out int total))
                return string.Empty;

            if (total == 0)
                return string.Empty;

            if (!int.TryParse(values[0].ToString(), out int current))
                return string.Empty;

            int percentage = (int)(100.0 / total * current);

            return string.Format("{0}% ({1}/{2})", percentage, current, total);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
