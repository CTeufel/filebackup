﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace FileBackupSystemTray.Converters
{
    public class ToMegaBytesValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!long.TryParse(value.ToString(), out long bytes))
                return string.Empty;

            return string.Format("{0}MB", (bytes / 1000000.0).ToString("N2"));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
