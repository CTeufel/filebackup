﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows;
using System.Windows.Interop;
using System.Drawing;
using System.IO;

namespace FileBackupSystemTray.Images
{
    internal static class ImageCache
    {
        enum ESeverity
        {
            Error = 0
        }

        static IDictionary<string, ImageSource> _imageSourceMap;

        private static IDictionary<string, ImageSource> ImageSourceMap 
        { 
            get 
            {
                if (_imageSourceMap == null)
                    _imageSourceMap = new Dictionary<string, ImageSource>();

                return _imageSourceMap;
            }
        }

        internal static ImageSource ExtractImageSourceFromFileExtension (string fileName)
        {
            var extension = Path.GetExtension(fileName);

            if (string.IsNullOrEmpty(extension))
                return null;

            return GetImageSource(fileName, extension);
        }        

        internal static ImageSource GetErrorImage()
        {
            return GetImageSource(SystemIcons.Error, ESeverity.Error.ToString());
        }

        private static ImageSource GetImageSource(Icon icon, string extension)
        {
            ImageSource imageSource;

            if (ImageSourceMap.TryGetValue(extension, out imageSource))
                return imageSource;

            return GetImageSourceFromIcon(icon, extension);
        }        

        private static ImageSource GetImageSource(string fileName, string extension)
        {
            ImageSource imageSource;

            if (ImageSourceMap.TryGetValue(extension, out imageSource))
                return imageSource;

            return GetImageSourceFromIcon(Icon.ExtractAssociatedIcon(fileName), extension);
        }

        private static ImageSource GetImageSourceFromIcon(Icon icon, string extension)
        {
            if (icon == null)
                icon = SystemIcons.Application;

            var imageSource = Imaging.CreateBitmapSourceFromHIcon(icon.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());

            ImageSourceMap.Add(extension, imageSource);

            return imageSource;
        }
    }
}
