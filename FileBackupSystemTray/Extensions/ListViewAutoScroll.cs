﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace FileBackupSystemTray.Extensions
{
    public class ListViewAutoScroll
    { 
        public static readonly DependencyProperty ScrollToEndProperty =
        DependencyProperty.RegisterAttached("ScrollToEnd", typeof(bool), typeof(ListViewAutoScroll), new UIPropertyMetadata(false, OnScrollToEnd));

        public static bool GetScrollToEnd(DependencyObject obj)
        {
            return (bool)obj.GetValue(ScrollToEndProperty);
        }

        public static void SetScrollToEnd(DependencyObject obj, bool value)
        {
            obj.SetValue(ScrollToEndProperty, value);
        }
        
        private static NotifyCollectionChangedEventHandler ScrollToEndEventHandler { get; set; }

        private static void OnScrollToEnd(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var scrollToEnd = (bool)e.NewValue;

            var listView = d as ListView;

            if (listView == null )
                return;

            if (listView.Items == null)
                return;

            var listViewItems = listView.Items;

            var notifyCollectionChanged = listView.Items.SourceCollection as INotifyCollectionChanged;

            if (notifyCollectionChanged == null)
                return;

            if (ScrollToEndEventHandler == null)
            {
                ScrollToEndEventHandler = new NotifyCollectionChangedEventHandler((s, arg) =>
                                                    {
                                                        if (listView.Items.Count > 0 && scrollToEnd)
                                                        {
                                                            var lastItem = listView.Items.Cast<object>().Last();
                                                            listViewItems.MoveCurrentTo(lastItem);
                                                            listView.ScrollIntoView(lastItem);
                                                        }
                                                    });
            }

            if (scrollToEnd)
            {
                notifyCollectionChanged.CollectionChanged += ScrollToEndEventHandler;
            }
            else
            {
                notifyCollectionChanged.CollectionChanged -= ScrollToEndEventHandler;
            }
            
        }
    }
}
