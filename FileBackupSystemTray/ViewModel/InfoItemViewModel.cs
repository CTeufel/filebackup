﻿using FileBackupManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace FileBackupSystemTray.ViewModel
{
    public class InfoItemViewModel : ItemViewModel
    {
        public InfoItemViewModel(string name)
        {
            Name = name;
        }

        #region Properties

        private string _name;

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                NotifyPropertyChanged();
            }
        }

        #endregion

        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            return string.Equals(Name, obj.ToString());
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
