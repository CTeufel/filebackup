﻿using FileBackupManager;
using FileBackupManager.CustomEventArgs;
using FileBackupSystemTray.Images;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace FileBackupSystemTray.ViewModel
{
    public class BackupItemViewModel : ItemViewModel
    {
        public BackupItemViewModel(IBackupItem backupItem)
        {
            BackupItem = backupItem;

            Name = backupItem.TargetFile;

            Number = backupItem.Number;

            Skipped = backupItem.Skipped;

            HasError = backupItem.HasError;

            if (HasError)
            {
                Error = backupItem.Error.ToString();
            }

            BackupItem.OnProgress += BackupItem_OnProgress;
        }

        private void BackupItem_OnProgress(IBackupItem sender, ProgressBackupItemEventArgs args)
        {
            Progress = args.Progress;
        }

        #region Properties

        private IBackupItem BackupItem { get; set; }

        private ImageSource _image;

        public ImageSource Image
        {
            get
            {
                if (_image != null)
                    return _image;

                if (HasError)
                    _image = ImageCache.GetErrorImage();
                else
                    _image = ImageCache.ExtractImageSourceFromFileExtension(BackupItem.SourceFile.FullName);

                return _image;
            }
            set
            {
                _image = value;
                NotifyPropertyChanged();
            }
        }

        private string _name;

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                NotifyPropertyChanged();
            }
        }

        private int _number;

        public int Number
        {
            get => _number;
            set
            {
                _number = value;
                NotifyPropertyChanged();
            }
        }

        private int _progress;

        public int Progress
        {
            get => _progress;
            set
            {
                _progress = value;
                NotifyPropertyChanged();
            }
        }

        private bool _skipped;

        public bool Skipped
        {
            get => _skipped;
            set
            {
                _skipped = value;
                NotifyPropertyChanged();
            }
        }

        private bool _hasError;

        public bool HasError
        {
            get => _hasError;
            set
            {
                _hasError = value;
                NotifyPropertyChanged();
            }
        }

        private string _error;

        public string Error
        {
            get => _error;
            set
            {
                _error = value;
                NotifyPropertyChanged();
            }
        }

        #endregion

        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            return string.Equals(Name, obj.ToString());
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
