﻿using FileBackupManager;
using FileBackupManager.CustomEventArgs;
using FileBackupSystemTray.Commands;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FileBackupSystemTray.ViewModel
{
    public class BackupControlViewModel : ViewModelBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public BackupControlViewModel()
        {
            UiSyncContext = SynchronizationContext.Current;

            Items = new ObservableCollection<ItemViewModel>();

            CurrentProgress = 0;
            MaximumProgress = 0;
            TotalBytesBackup = 0;
            CacheTargetPathContent = true;
            UseEncryption = true;
            Autoscroll = true;

#if DEBUG
            SourcePath = @"c:\temp\test";
            DestinationPath = @"c:\temp\testTargetPath";
            Password = "123";
#endif

            BackupManager.OnStartBackup += BackupManager_OnStartBackup;
            BackupManager.OnCacheDestinationPathContent += BackupManager_OnCacheDestinationPathContent;
            BackupManager.OnStopBackup += BackupManager_OnStopBackup;
            BackupManager.OnPauseBackup += BackupManager_OnPauseBackup;
            BackupManager.OnContinueBackup += BackupManager_OnContinueBackup;
            BackupManager.OnDoneBackup += BackupManager_OnDoneBackup;
            BackupManager.OnBeforeBackupItem += BackupManager_OnBeforeBackupItem;
            BackupManager.OnAfterBackupItem += BackupManager_OnAfterBackupItem;
        }        

        #region Properties   

        long _totalBytesBackup;

        public long TotalBytesBackup
        {
            get { return _totalBytesBackup; }
            set
            {
                _totalBytesBackup = value;

                NotifyPropertyChanged();
            }
        }

        int _currentProgress;

        public int CurrentProgress
        {
            get { return _currentProgress; }
            set
            {
                _currentProgress = value;

                NotifyPropertyChanged();
            }
        }

        int _maximumProgress;

        public int MaximumProgress
        {
            get { return _maximumProgress; }
            set
            {
                _maximumProgress = value;

                NotifyPropertyChanged();
            }
        }

        IList<ItemViewModel> _items;

        public IList<ItemViewModel> Items
        {
            get { return _items; }
            set
            {
                if (_items == null)
                    _items = value;

                NotifyPropertyChanged();
            }
        }

        private string _sourcePath;

        public string SourcePath
        {
            get => _sourcePath;
            set
            {
                _sourcePath = value;
                NotifyPropertyChanged();
            }
        }

        private string _destinationPath;

        public string DestinationPath
        {
            get => _destinationPath;
            set
            {
                _destinationPath = value;
                NotifyPropertyChanged();
            }
        }

        bool _useEncryption;

        public bool UseEncryption
        {
            get { return _useEncryption; }
            set
            {
                _useEncryption = value;
                NotifyPropertyChanged();
            }
        }

        bool _overwrite;

        public bool Overwrite
        {
            get { return _overwrite; }
            set
            {
                _overwrite = value;
                NotifyPropertyChanged();
            }
        }

        bool _cacheTargetPathContent;

        public bool CacheTargetPathContent
        {
            get { return _cacheTargetPathContent; }
            set
            {
                _cacheTargetPathContent = value;
                NotifyPropertyChanged();
            }
        }

        

        bool _autoscroll;

        public bool Autoscroll
        {
            get { return _autoscroll; }
            set
            {
                _autoscroll = value;
                NotifyPropertyChanged();
            }
        }

        public string Password
        {
            get
            {
                return new NetworkCredential(string.Empty, Secret).Password;
            }
            set
            {
                try
                {
                    Secret = new NetworkCredential(string.Empty, value).SecurePassword;
                }
                catch (Exception e)
                {
                    Logger.Error(e);
                }

                NotifyPropertyChanged();
            }
        }

        SecureString Secret { get; set; }

        SynchronizationContext UiSyncContext { get; set; }

        IBackupManager _backupManager;

        private IBackupManager BackupManager
        {
            get
            {
                if (_backupManager == null)
                    _backupManager = new BackupManager();
                return _backupManager;
            }
        }

        #endregion

        #region Commands
        private ICommand _startBackupCommand;

        public ICommand StartBackupCommand
        {
            get
            {
                if (null == _startBackupCommand)
                    _startBackupCommand = new RelayCommand(ExecuteStartBackupCommand, CanExecuteStartBackupCommand);

                return _startBackupCommand;
            }
        }

        private void ExecuteStartBackupCommand(object obj)
        {
            Logger.Info("ExecuteStartBackupCommand", obj);

            if (BackupManager.State == EState.Stopped)
            {
                BackupManager.StartAsync(new System.IO.DirectoryInfo(SourcePath), new System.IO.DirectoryInfo(DestinationPath), UseEncryption ? Secret : null, Overwrite);
            }
            else if (BackupManager.State == EState.Paused)
            {
                BackupManager.ContinueAsync();
            }
        }

        private bool CanExecuteStartBackupCommand(object obj)
        {
            Logger.Info("ExecuteStartBackupCommand", obj);

            if (string.IsNullOrEmpty(SourcePath))
                return false;

            if (string.IsNullOrEmpty(DestinationPath))
                return false;

            if (UseEncryption && (string.IsNullOrEmpty(Password) || Secret == null))
                return false;

            return BackupManager.State != EState.Running;
        }

        private ICommand _pauseBackupCommand;

        public ICommand PauseBackupCommand
        {
            get
            {
                if (null == _pauseBackupCommand)
                    _pauseBackupCommand = new RelayCommand(ExecutePauseBackupCommand, CanExecutePauseBackupCommand);

                return _pauseBackupCommand;
            }
        }

        private void ExecutePauseBackupCommand(object obj)
        {
            Logger.Info("ExecutePauseBackupCommand", obj);

            BackupManager.PauseAsync();
        }

        private bool CanExecutePauseBackupCommand(object obj)
        {
            Logger.Info("CanExecutePauseBackupCommand", obj);

            return BackupManager.State == EState.Running;
        }

        private ICommand _stopBackupCommand;

        public ICommand StopBackupCommand
        {
            get
            {
                if (null == _stopBackupCommand)
                    _stopBackupCommand = new RelayCommand(ExecuteStopBackupCommand, CanExecuteStopBackupCommand);

                return _stopBackupCommand;
            }
        }

        private void ExecuteStopBackupCommand(object obj)
        {
            Logger.Info("ExecuteStopBackupCommand", obj);

            BackupManager.StopAsync();
        }

        private bool CanExecuteStopBackupCommand(object obj)
        {
            Logger.Info("CanExecuteStopBackupCommand", obj);

            return BackupManager.State == EState.Running || BackupManager.State == EState.Paused;
        }

        #endregion

        #region Event Handlers


        private void BackupManager_OnAfterBackupItem(IBackupManager sender, AfterEventArgs args)
        {
            UiSyncContext.Post(o => UpdateTotalProgress(args), null);
        }

        private void UpdateTotalProgress(AfterEventArgs args)
        {
            MaximumProgress = args.Total;

            CurrentProgress = args.Current;

            TotalBytesBackup += args.BackupItem != null ? args.BackupItem.TargetFileBytes : 0;
        }


        private void BackupManager_OnBeforeBackupItem(IBackupManager sender, BeforeEventArgs args)
        {
            AddBackupItemInUiThread(args.BackupItem);
        }

        private void BackupManager_OnContinueBackup(object sender, EventArgs e)
        {
            AddInfoItemInUiThread("Backup resumed");
        }

        private void BackupManager_OnPauseBackup(object sender, EventArgs e)
        {
            AddInfoItemInUiThread("Backup paused");
        }

        private void BackupManager_OnStopBackup(object sender, EventArgs e)
        {
            AddInfoItemInUiThread("Backup stopped");
        }

        private void BackupManager_OnStartBackup(object sender, EventArgs e)
        {
            UiSyncContext.Post(o => StartBackup(), null);
        }

        private void BackupManager_OnCacheDestinationPathContent(object sender, EventArgs e)
        {
            AddInfoItemInUiThread("Reading destination folder file list in one shot. This could take a while...");
        }

        private void BackupManager_OnDoneBackup(object sender, EventArgs e)
        {
            AddInfoItemInUiThread("Backup done", true);
        }

        private void StartBackup()
        {
            ResetBackup();

            Items.Add(new InfoItemViewModel("Backup started"));
        }

        private void ResetBackup()
        {
            MaximumProgress = 0;

            CurrentProgress = 0;

            TotalBytesBackup = 0;

            Items.Clear();
        }

        private void AddInfoItemInUiThread(string text, bool updateCommands = false)
        {
            UiSyncContext.Post(o =>
                {
                    if (updateCommands)
                    {
                        ((RelayCommand)StartBackupCommand).RaiseCanExecuteChanged();

                        ((RelayCommand)StopBackupCommand).RaiseCanExecuteChanged();

                        ((RelayCommand)PauseBackupCommand).RaiseCanExecuteChanged();
                    }

                    Items.Add(new InfoItemViewModel(text));
                }, null);
        }

        private void AddBackupItemInUiThread(IBackupItem backupItem)
        {
            UiSyncContext.Post(o =>
                {
                    var lastInsertedItem = Items.LastOrDefault();

                    var backItemViewModel = new BackupItemViewModel(backupItem);

                    if (!backItemViewModel.Equals(lastInsertedItem))
                        Items.Add(backItemViewModel);
                }, null);
        }

        #endregion
    }
}
