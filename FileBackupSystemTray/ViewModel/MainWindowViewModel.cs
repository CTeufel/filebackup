﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using FileBackupSystemTray.Commands;

namespace FileBackupSystemTray.ViewModel
{
    public class MainWindowViewModel: ViewModelBase
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public MainWindowViewModel()
        {
            BackupControlViewModel = new BackupControlViewModel();
        }

        #region Commands
        private ICommand _closeApplicationCommand;

        public ICommand CloseApplicationCommand
        {
            get
            {
                if (null == _closeApplicationCommand)
                    _closeApplicationCommand = new RelayCommand(ExecuteCloseApplicationCommand);

                return _closeApplicationCommand;
            }
        }
        private void ExecuteCloseApplicationCommand(object obj)
        {
            Logger.Info("ExecuteCloseApplicationCommand", obj);

            Application.Current.Shutdown();
        }

        #endregion

        #region Properties

        public BackupControlViewModel BackupControlViewModel { get; set; }

        #endregion
    }

}
