﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Threading;
using FileBackupManager;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FileBackupServiceTest
{
    [TestClass]
    public class BackupManagerTest
    {
        BackupManager BackupManager { get; set; }

        SecureString Secret { get { return new NetworkCredential(string.Empty, "123").SecurePassword; } }

        DirectoryInfo SourcePath { get { return new DirectoryInfo(@"c:\temp\test"); } }

        string TargetPathPrefix { get { return @".\test\output-"; } }

        [TestMethod]
        public void BackupManagerStartTest()
        {
            BackupManager = new BackupManager();

            //var targetPath = new DirectoryInfo(@"Z:\Onlinespeicher\Test");

            var targetPath = BuildTargetPath(new Guid("16CB73FDFF5647B492187F905154DE1E"));

            CleanTarget(targetPath);

            bool wait = true;

            BackupManager = new BackupManager();

            BackupManager.OnStopBackup += (sender, args) =>
            {

                try
                {
                    var sourceFiles = SourcePath.GetFiles("*.*", SearchOption.AllDirectories);

                    var targetFiles = targetPath.GetFiles("*.*", SearchOption.AllDirectories);

                    Assert.IsTrue(sourceFiles.Length == targetFiles.Length, "Number of files {0} in target does not match source {1}!", targetFiles.Length, sourceFiles.Length);

                    sourceFiles.ToList().ForEach(sourceFile =>
                    {
                        var sourceFileWithoutSourcePath = sourceFile.FullName.Replace(SourcePath.FullName, string.Empty);
                        var exists = false;
                        foreach (var targetFile in targetFiles.ToList())
                        {
                            var targetFileWithoutPath = targetFile.FullName.Replace(targetPath.FullName, string.Empty);
                            if (sourceFileWithoutSourcePath.Equals(targetFileWithoutPath.TrimEnd(".zip".ToCharArray()), StringComparison.InvariantCultureIgnoreCase))
                            {
                                exists = true;
                                break;
                            }
                        }
                        Assert.IsTrue(exists, "Source file {0} does not exist in target path!", sourceFile);
                    });
                }
                finally
                {
                    wait = false;
                }
            };

            BackupManager.StartAsync(SourcePath, targetPath, Secret);

            var maxWaitTime = 60000;
            var waitTime = 0;

            while (wait && waitTime <= maxWaitTime)
            {
                Thread.Sleep(1000);

                waitTime += 1000;
            }
        }

        [TestMethod]
        public void BackupManagerStopTest()
        {
            var targetPath = BuildTargetPath(new Guid("29BB6F2A0CAC4FB88A96D621E953789D"));

            CleanTarget(targetPath);

            bool wait = true;

            bool stopReached = false;

            BackupManager = new BackupManager();

            BackupManager.OnStopBackup += (sender, args) =>
            {
                try
                {
                    Assert.IsTrue(((IBackupManager)sender).State == EState.Stopped);

                    stopReached = true;
                }
                finally
                {
                    wait = false;
                }
            };

            BackupManager.StartAsync(SourcePath, targetPath, Secret);

            var maxWaitTime = 10000;
            var waitTime = 0;

            while (wait && waitTime <= maxWaitTime)
            {
                Thread.Sleep(1000);

                BackupManager.StopAsync();

                Thread.Sleep(1000);

                waitTime += 2000;
            }

            Assert.IsTrue(stopReached);
        }

        [TestMethod]
        public void BackupManagerPauseTest()
        {
            var targetPath = BuildTargetPath(new Guid("454150D4DB1F4A408F125C822FC53B54"));

            CleanTarget(targetPath);

            bool wait = true;

            bool pausedReached = false;

            BackupManager = new BackupManager();

            BackupManager.OnPauseBackup += (sender, args) =>
            {
                try
                {
                    Assert.IsTrue(((IBackupManager)sender).State == EState.Paused);

                    pausedReached = true;
                }
                finally
                {
                    wait = false;
                }
            };

            BackupManager.StartAsync(SourcePath, targetPath, Secret);

            var maxWaitTime = 10000;
            var waitTime = 0;

            while (wait && waitTime <= maxWaitTime)
            {
                Thread.Sleep(1000);

                BackupManager.PauseAsync();

                Thread.Sleep(1000);

                waitTime += 2000;
            }

            Assert.IsTrue(pausedReached);

            BackupManager.StopAsync();

            Thread.Sleep(2000);
        }

        [TestMethod]
        public void BackupManagerContinueTest()
        {
            var targetPath = BuildTargetPath(new Guid("454150D4DB1F4A408F125C822FC53B54"));

            CleanTarget(targetPath);

            bool wait = true;

            bool continueReached = false;

            BackupManager = new BackupManager();

            BackupManager.OnContinueBackup += (sender, args) =>
            {
                try
                {
                    Assert.IsTrue(((IBackupManager)sender).State == EState.Running);

                    continueReached = true;
                }
                finally
                {
                    wait = false;
                }
            };

            BackupManager.StartAsync(SourcePath, targetPath, Secret);

            var maxWaitTime = 10000;
            var waitTime = 0;

            Thread.Sleep(2000);

            BackupManager.PauseAsync();

            while (wait && waitTime <= maxWaitTime)
            {
                Thread.Sleep(5000);

                BackupManager.ContinueAsync();

                waitTime += 5000;
            }

            Assert.IsTrue(continueReached);

            BackupManager.StopAsync();

            Thread.Sleep(2000);
        }

        [TestMethod]
        public void BackupManagerAfterBackupItemTest()
        {
            var targetPath = BuildTargetPath(new Guid("454150D4DB1F4A408F125C822FC53B33"));

            CleanTarget(targetPath);

            bool progressReached = false;
            bool totalProgressReached = false;

            BackupManager = new BackupManager();

            BackupManager.OnAfterBackupItem += (sender, args) =>
            {
                progressReached = true;

                totalProgressReached = args.Current == args.Total;
            };

            BackupManager.StartAsync(SourcePath, targetPath, Secret);

            var maxWaitTime = 60000;
            var waitTime = 0;

            while (waitTime <= maxWaitTime)
            {
                Thread.Sleep(1000);

                if (progressReached && totalProgressReached)
                    break;

                waitTime += 1000;
            }

            Assert.IsTrue(progressReached);

            Assert.IsTrue(totalProgressReached);
        }

        [TestMethod]
        public void BackupManagerBeforeBackupItemTest()
        {
            var targetPath = BuildTargetPath(new Guid("454150D4DB1F4A408F125C822FC53B66"));

            CleanTarget(targetPath);

            bool progressBackupItemReached = false;
            bool totalProgressReached = false;

            BackupManager = new BackupManager();

            BackupManager.OnBeforeBackupItem += (sender, args) =>
            {
                progressBackupItemReached = true;

                Assert.IsFalse(args.BackupItem.HasError);

                args.BackupItem.OnProgress += (backupItem, eventArgs) =>
                {
                    if (eventArgs.Progress == eventArgs.Total)
                        totalProgressReached = true;
                };                
            };

            BackupManager.StartAsync(SourcePath, targetPath, Secret);

            var maxWaitTime = 60000;
            var waitTime = 0;

            while (waitTime <= maxWaitTime)
            {
                Thread.Sleep(1000);

                if (progressBackupItemReached && totalProgressReached)
                    break;

                waitTime += 1000;
            }

            Assert.IsTrue(progressBackupItemReached);

            Assert.IsTrue(totalProgressReached);
        }

        private void CleanTarget(DirectoryInfo targetPath)
        {
            if (targetPath.Exists)
            {
                try
                {
                    Directory.Delete(targetPath.FullName, true);
                }
                catch (Exception e)
                {
                    Assert.Fail("Unable to delete target directory! " + e);
                }
            }
        }

        private DirectoryInfo BuildTargetPath(Guid guid)
        {
            return new DirectoryInfo(TargetPathPrefix + guid.ToString());
        }
    }
}
