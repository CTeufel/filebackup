﻿using FileBackupManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace FileBackupService
{
    partial class ServiceHost : ServiceBase
    {
        private IFileBackupController FileBackupControllerInstance { get; set; }

        public ServiceHost()
        {
            InitializeComponent();

            // As admin; Compile and run once or execute install.bat file to get this to work. 
            // Proper workaround would be an 
            // installation script adding the eventlog source in win registry.

            if (!EventLog.SourceExists(Resources.ResourceProvider.ServiceSource))
            {
                EventLog.CreateEventSource(
                    Resources.ResourceProvider.ServiceSource, Resources.ResourceProvider.ServiceLog);
            }
            EventLogInstance.Source = Resources.ResourceProvider.ServiceSource;
            EventLogInstance.Log = Resources.ResourceProvider.ServiceLog;

            FileBackupControllerInstance = new FileBackupController();
        }
              

        protected override void OnStart(string[] args)
        {
#if DEBUG
            Debugger.Launch();
#endif

            EventLogInstance.WriteEntry("In OnStart. serviceArgs=" + args == null ? "<none>" : String.Join(", ", args));

            var localArgs = GetArgsFallback(args);

            DirectoryInfo source = null;

            try
            {
                source = new DirectoryInfo(localArgs.First());
            }
            catch (Exception e)
            {
                EventLogInstance.WriteEntry(e.ToString(), EventLogEntryType.Error);
            }

            DirectoryInfo target = null;

            try
            {
                target = new DirectoryInfo(localArgs[1]);
            }
            catch (Exception e)
            {
                EventLogInstance.WriteEntry(e.ToString(), EventLogEntryType.Error);
            }

            SecureString secret = null;

            try
            {
                secret = new NetworkCredential(string.Empty, localArgs[2]).SecurePassword;
            }
            catch (Exception e)
            {
                EventLogInstance.WriteEntry(e.ToString(), EventLogEntryType.Error);
            }

            double breakTimeInHours = 1;

            try
            {
                breakTimeInHours = double.Parse(localArgs.Last(), NumberStyles.AllowDecimalPoint, new CultureInfo("en-EN"));
            }
            catch (Exception e)
            {
                EventLogInstance.WriteEntry(e.ToString(), EventLogEntryType.Error);
            }

            if (source != null && target != null && secret != null)
            {
                FileBackupControllerInstance.RunAsync(source, target, secret, breakTimeInHours);
            }
            else
            {
                throw new ArgumentException("Unable to read (command line) arguments.");
            }

            base.OnStart(args);
        }       

        protected override void OnStop()
        {
            EventLogInstance.WriteEntry("In OnStop.");

            FileBackupControllerInstance.StopAsync();

            base.OnStop();
        }

        protected override void OnContinue()
        {
            EventLogInstance.WriteEntry("In OnContinue.");

            FileBackupControllerInstance.ContinueAsync();

            base.OnContinue();
        }

        protected override void OnPause()
        {
            EventLogInstance.WriteEntry("In OnPause.");

            FileBackupControllerInstance.PauseAsync();

            base.OnPause();
        }

        protected override void OnShutdown()
        {
            EventLogInstance.WriteEntry("In OnShutdown.");

            FileBackupControllerInstance.StopAsync();

            base.OnShutdown();
        }

        protected override bool OnPowerEvent(PowerBroadcastStatus powerStatus)
        {
            EventLogInstance.WriteEntry("In OnPowerEvent. " + powerStatus);

            FileBackupControllerInstance.StopAsync();

            return base.OnPowerEvent(powerStatus);
        }

        /*
        protected override void OnSessionChange(SessionChangeDescription changeDescription)
        {
            EventLogInstance.WriteEntry("In OnSessionChange. " + changeDescription);

            //TODO

            base.OnSessionChange(changeDescription);
        }
        */

        private static string[] GetArgsFallback(string[] args)
        {
            if (args != null && args.Length == 4)
                return args;

            // Fallback, try read App.Config
            var sourcePath = ConfigurationManager.AppSettings["SourcePath"];
            var targetPath = ConfigurationManager.AppSettings["TargetPath"];
            var password = ConfigurationManager.AppSettings["Password"];
            var breakTimeInHours = ConfigurationManager.AppSettings["BreakTimeInHours"];
            

            if (string.IsNullOrEmpty(sourcePath) || string.IsNullOrEmpty(targetPath) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(breakTimeInHours))
            {
#if DEBUG
                sourcePath = @"c:\temp\test";
                targetPath = @"c:\temp\testTargetPath";
                password = "123";
                breakTimeInHours = "1";
#else
                throw new ArgumentException("Please specifiy SourcePath, TargetPath and Password.");
#endif
            }

            return new string[] { sourcePath, targetPath, password, breakTimeInHours };
        }
    }
}
