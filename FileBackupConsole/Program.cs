﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using FileBackupManager;
using System.Threading;
using Microsoft.Win32;

namespace FileBackupConsole
{
    class Program
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            Logger.Info("In OnStart. serviceArgs=" + args == null ? "<none>" : String.Join(", ", args));

            var localArgs = GetArgsFallback(args);

            DirectoryInfo source = null;

            try
            {
                source = new DirectoryInfo(localArgs.First());
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }

            DirectoryInfo target = null;

            try
            {
                target = new DirectoryInfo(localArgs[1]);
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }

            SecureString secret = null;

            try
            {
                secret = new NetworkCredential(string.Empty, localArgs[2]).SecurePassword;
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }

            double breakTimeInHours = 1;

            try
            {
                breakTimeInHours = double.Parse(localArgs.Last(), NumberStyles.AllowDecimalPoint, new CultureInfo("en-EN"));
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }

            if (source != null && target != null && secret != null)
            {
                Run(source, target, secret, breakTimeInHours);
            }
            else
            {
                throw new ArgumentException("Unable to read (command line) arguments.");
            }
        }

        private static void Run(DirectoryInfo source, DirectoryInfo target, SecureString secret, double breakTimeInHours)
        {
            Logger.Info("source = {0}, target = {0}, breakTimeInHours = {2}", source, target, breakTimeInHours);

            try
            {
                bool canRun = true;

                IFileBackupController fileBackupController = new FileBackupController();

                AppDomain.CurrentDomain.ProcessExit += (sender, e) =>
                                        {
                                            fileBackupController.StopAsync();

                                            canRun = false;
                                        };

                SystemEvents.SessionEnded += (sender, e) =>
                                        {
                                            fileBackupController.StopAsync();

                                            canRun = false;
                                        };


                fileBackupController.RunAsync(source, target, secret, breakTimeInHours);

                while (canRun)
                {
                    Thread.Sleep(2000);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
        }

        private static string[] GetArgsFallback(string[] args)
        {
            if (args != null && args.Length == 4)
                return args;

            // Fallback, try read App.Config
            var sourcePath = ConfigurationManager.AppSettings["SourcePath"];
            var targetPath = ConfigurationManager.AppSettings["TargetPath"];
            var password = ConfigurationManager.AppSettings["Password"];
            var breakTimeInHours = ConfigurationManager.AppSettings["BreakTimeInHours"];


            if (string.IsNullOrEmpty(sourcePath) || string.IsNullOrEmpty(targetPath) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(breakTimeInHours))
            {
#if DEBUG
                sourcePath = @"c:\temp\test";
                targetPath = @"c:\temp\testTargetPath";
                password = "123";
                breakTimeInHours = "1";
#else
                throw new ArgumentException("Please specifiy SourcePath, TargetPath and Password.");
#endif
            }

            return new string[] { sourcePath, targetPath, password, breakTimeInHours };
        }
    }
}
