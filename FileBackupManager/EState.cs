﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileBackupManager
{
    public enum EState
    {
        Stopped, //Default
        Running,        
        Paused
    }
}
