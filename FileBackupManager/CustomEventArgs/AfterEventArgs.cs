﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileBackupManager.CustomEventArgs
{
    public class AfterEventArgs : EventArgs
    {
        public AfterEventArgs(int total, int current, IBackupItem backupItem)
        {
            Total = total;
            Current = current;
            BackupItem = backupItem;
        }

        public int Total { get; private set; }

        public int Current { get; private set; }

        public IBackupItem BackupItem { get; private set; }
    }
}
