﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileBackupManager.CustomEventArgs
{
    public class BeforeEventArgs : EventArgs
    {
        public BeforeEventArgs(IBackupItem backupItem)
        {
            BackupItem = backupItem;
        }

        public IBackupItem BackupItem { get; private set; }
    }
}
