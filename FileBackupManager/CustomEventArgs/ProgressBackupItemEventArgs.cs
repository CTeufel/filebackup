﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileBackupManager.CustomEventArgs
{
    public class ProgressBackupItemEventArgs : EventArgs
    {
        public ProgressBackupItemEventArgs(IBackupItem backupItem, int progress)
        {
            BackupItem = backupItem;

            Progress = progress;             
        }

        public IBackupItem BackupItem { get; private set; }

        public int Progress { get; }

        public int Total { get { return 100; } }
    }
}
