﻿using FileBackupManager.CustomEventArgs;
using Ionic.Zip;
using Ionic.Zlib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace FileBackupManager
{
    public class BackupItem : IBackupItem
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public BackupItem(int number, FileInfo sourceFile, DirectoryInfo sourcePath, DirectoryInfo targetPath)
        {
            Initialize(number, sourceFile, sourcePath, targetPath);
        }

        public event ProgressBackupItemEventHandler OnProgress;

        public bool Skipped { get; private set; }

        public int Number { get; private set; }

        public string TargetFile { get; private set; }

        public Exception Error { get; private set; }

        public bool HasError { get { return Error != null; } }

        public FileInfo SourceFile { get; private set; }

        public DirectoryInfo SourcePath { get; private set; }

        public DirectoryInfo TargetPath { get; private set; }

        public string TargetFileDirectory { get; private set; }

        public long TargetFileBytes { get; private set; }

        public void DoBackup(SecureString secret, bool overwrite = false, FileInfo[] targetPathFiles = null)
        {
            Logger.Info("DoBackup overwrite = {0}", overwrite);

            int progress = 0;

            try
            {
                OnProgress?.Invoke(this, new ProgressBackupItemEventArgs(this, progress));

                if (!Directory.Exists(TargetFileDirectory))
                    Directory.CreateDirectory(TargetFileDirectory);

                var targetZipFile = string.Format("{0}.zip", TargetFile);

                var targetFileExists = false;

                // Target path contents cached or not?                
                if (targetPathFiles == null) // Not chached
                {
                    targetFileExists = File.Exists(targetZipFile);
                }
                else // cached - read from cache and check if file exists.
                {
                    targetFileExists = targetPathFiles.Any(f => f.FullName.Equals(targetZipFile, StringComparison.InvariantCultureIgnoreCase));
                }

                if (targetFileExists)
                {
                    if (!overwrite) // Nothing to do, do not overwrite and skip
                    {
                        Skipped = true;

                        return;
                    }
                    File.Delete(TargetFile); // delete, as target exists and overwrite == true
                }

                CreateZip(secret, targetZipFile);
            }
            catch (Exception e)
            {
                Logger.Error(e);

                Error = e;

                OnProgress?.Invoke(this, new ProgressBackupItemEventArgs(this, 0));
            }              
            finally
            {
                if (!HasError)
                {
                    OnProgress?.Invoke(this, new ProgressBackupItemEventArgs(this, 100));
                }
            }            
        }

        private void CreateZip(SecureString secret, string targetZipFile)
        {
            Logger.Info("CreateZip targetZipFile = {0}", targetZipFile);

            using (ZipFile zip = new ZipFile())
            {
                zip.CompressionLevel = CompressionLevel.BestCompression;

                zip.CompressionMethod = CompressionMethod.BZip2;

                if (secret != null)
                {
                    zip.Password = new NetworkCredential(string.Empty, secret).Password;

                    zip.Encryption = EncryptionAlgorithm.WinZipAes256;
                }

                zip.AddFile(SourceFile.FullName, string.Empty);

                zip.SaveProgress += (sender, eventArgs) =>
                {
                    switch (eventArgs.EventType)
                    {
                        case ZipProgressEventType.Saving_EntryBytesRead:
                            var progress = (int)((100.0 / eventArgs.TotalBytesToTransfer) * eventArgs.BytesTransferred);

                            OnProgress?.Invoke(this, new ProgressBackupItemEventArgs(this, progress));

                            break;
                        default:
                            break;
                    }
                };

                zip.Save(targetZipFile);

                TargetFileBytes = new FileInfo(targetZipFile).Length;

                OnProgress?.Invoke(this, new ProgressBackupItemEventArgs(this, 100));
            }
        }

        private void Initialize(int number, FileInfo sourceFile, DirectoryInfo sourcePath, DirectoryInfo targetPath)
        {
            Logger.Info("Initialize number={0} - sourceFile={1} - sourcePath={2} - targetPath={3}", number, sourceFile, sourcePath, targetPath);

            Number = number;

            SourceFile = sourceFile;

            SourcePath = sourcePath;

            TargetPath = targetPath;

            // Think - there must be an easier way to do the below lines of code and build up the target path.
            var trimmedTargetPath = sourceFile.Directory.FullName.Substring(sourcePath.FullName.Length, (sourceFile.Directory.FullName.Length - sourcePath.FullName.Length));

            var directorySeparator = Path.DirectorySeparatorChar.ToString();
            TargetFileDirectory = string.Format("{0}{1}{2}",
                targetPath.FullName,
                trimmedTargetPath.StartsWith(directorySeparator) || targetPath.FullName.EndsWith(directorySeparator) ? string.Empty : directorySeparator,
                trimmedTargetPath);

            TargetFile = string.Format("{0}{1}{2}",
                TargetFileDirectory,
                TargetFileDirectory.EndsWith(directorySeparator) || sourceFile.Name.StartsWith(directorySeparator) ? string.Empty : directorySeparator,
                sourceFile.Name);
        }

        #region Overrides

        public override bool Equals(object obj)
        {
            return obj is BackupItem item &&
                   TargetFile == item.TargetFile;
        }

        public override int GetHashCode()
        {
            return 539060726 + EqualityComparer<string>.Default.GetHashCode(TargetFile);
        }

        #endregion
    }
}
