﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace FileBackupManager
{
    public interface IFileBackupController
    {
        void RunAsync(DirectoryInfo source, DirectoryInfo target, SecureString secret, double breakTimeInHours);

        void StopAsync();
        
        void ContinueAsync();

        void PauseAsync();
    }
}
