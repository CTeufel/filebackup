﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FileBackupManager
{
    public class FileBackupController : IFileBackupController
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        private static object Sync = new object();

        public FileBackupController()
        {
            BackupManagerInstance = new BackupManager();
        }

        private bool CanRun { get; set; }

        private BackupManager BackupManagerInstance { get; set; }

        public void RunAsync(DirectoryInfo source, DirectoryInfo target, SecureString secret, double breakTimeInHours)
        {
            lock (Sync)
            {
                Logger.Info("Start source = {0}, target = {1}, breakTimeInHours = {2}, CanRun = {3}", source, target, breakTimeInHours, CanRun);

                // Already running, nothing to do.
                if (CanRun)
                    return;

                CanRun = true;

                _ = Run(source, target, secret, breakTimeInHours);
            }
        }

        public void StopAsync()
        {
            lock (Sync)
            {
                Logger.Info("Stop");

                CanRun = false;

                BackupManagerInstance.StopAsync();
            }
        }

        public void ContinueAsync()
        {
            Logger.Info("Continue");

            BackupManagerInstance.ContinueAsync();
        }

        public void PauseAsync()
        {
            Logger.Info("Pause");

            BackupManagerInstance.PauseAsync();
        }

        private async Task Run(DirectoryInfo source, DirectoryInfo target, SecureString secret, double breakTimeInHours)
        {
            try
            {
                Logger.Debug("Run source = {0}, target = {1}", source, target);

                await Task.Run(() =>
                {
                    // Run always for the first time ignoring breakTimeInHours
                    var firstRun = true;

                    // Store start date to trigger backup every breakTimeInHours
                    var startDateTime = DateTime.Now;

                    // Set that service is started and can run.
                    CanRun = true;

                    while (CanRun)
                    {
                        if (firstRun)
                        {
                            Logger.Info("Run first backup");

                            BackupManagerInstance.StartAsync(source, target, secret, false, true);

                            firstRun = false;
                        }

                        // Sleep and wake up everyonce in a while to check for breaktime and trigger backup again.
                        Thread.Sleep(TimeSpan.FromMinutes(5));

                        var timeDiff = DateTime.Now - startDateTime;

                        if (BackupManagerInstance.State == EState.Stopped && timeDiff > TimeSpan.FromHours(breakTimeInHours))
                        {
                            Logger.Info("Woke up after {0} minutes. Run backup again.", timeDiff.TotalMinutes.ToString("{0:0.##}"));

                            startDateTime = DateTime.Now;

                            BackupManagerInstance.StartAsync(source, target, secret, false, true);
                        }
                    }

                });
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
        }
    }
}
