﻿using FileBackupManager.CustomEventArgs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace FileBackupManager
{
    public delegate void AfterBackupItemEventHandler(IBackupManager sender, AfterEventArgs args);

    public delegate void BeforeBackupItemEventHandler(IBackupManager sender, BeforeEventArgs args);

    public interface IBackupManager
    {
        void StartAsync(DirectoryInfo source, DirectoryInfo target, SecureString secret, bool overwrite = false, bool cacheTargetPathContent = true);

        void StopAsync();

        void PauseAsync();

        void ContinueAsync();

        event EventHandler OnStartBackup;

        event EventHandler OnCacheDestinationPathContent;

        event EventHandler OnStopBackup;

        event EventHandler OnPauseBackup;

        event EventHandler OnContinueBackup;

        event EventHandler OnDoneBackup;

        event BeforeBackupItemEventHandler OnBeforeBackupItem;

        event AfterBackupItemEventHandler OnAfterBackupItem;

        EState State { get; }
    }
}
