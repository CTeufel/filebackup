﻿using FileBackupManager.CustomEventArgs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace FileBackupManager
{
    public delegate void ProgressBackupItemEventHandler(IBackupItem sender, ProgressBackupItemEventArgs args);

    public interface IBackupItem
    {
        event ProgressBackupItemEventHandler OnProgress;

        bool Skipped { get; }

        int Number { get; }

        string TargetFile { get; }

        long TargetFileBytes { get; }

        FileInfo SourceFile { get; }

        DirectoryInfo SourcePath { get; }

        DirectoryInfo TargetPath { get; }

        Exception Error { get; }

        bool HasError { get; }        

        void DoBackup(SecureString secret, bool overwrite = false, FileInfo[] targetPathFiles = null);
    }
}
