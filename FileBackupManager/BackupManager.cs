﻿using FileBackupManager.CustomEventArgs;
using Ionic.Zip;
using Ionic.Zlib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FileBackupManager
{
    public class BackupManager : IBackupManager
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        private static object Sync = new object();

        public event EventHandler OnStartBackup;

        public event EventHandler OnCacheDestinationPathContent;

        public event EventHandler OnStopBackup;

        public event EventHandler OnPauseBackup;

        public event EventHandler OnContinueBackup;

        public event EventHandler OnDoneBackup;

        public event AfterBackupItemEventHandler OnAfterBackupItem;

        public event BeforeBackupItemEventHandler OnBeforeBackupItem;        

        public EState State { get; private set; }

        public void StartAsync(DirectoryInfo source, DirectoryInfo target, SecureString secret = null, bool overwrite = false, bool cacheTargetPathContent = false)
        {
            Logger.Debug(string.Format("StartAsync source={0} + target={1} + secret not empty = {2} + State = {3} + overwrite = {4} + cacheTargetPathContent = {5}", source, target, secret != null ? true.ToString() : false.ToString(), State, overwrite, cacheTargetPathContent));

            lock (Sync)
            {
                Logger.Debug("StartAsync State = {0}", State);

                if (State == EState.Running)
                    return;

                State = EState.Running;               

                OnStartBackup?.Invoke(this, EventArgs.Empty);

                _ = StartBackup(source, target, secret, overwrite, cacheTargetPathContent);
            }
        }

        public void StopAsync()
        {
            Logger.Debug("StopAsync - State = {0}", State);

            lock (Sync)
            {   
                if (State == EState.Stopped)
                    return;

                State = EState.Stopped;               
            }

            OnStopBackup?.Invoke(this, EventArgs.Empty);
        }

        public void ContinueAsync()
        {
            Logger.Debug("ContinueAsync - State = {0}", State);

            lock (Sync)
            {
                if (State == EState.Running)
                    return;

                State = EState.Running;                
            }

            OnContinueBackup?.Invoke(this, EventArgs.Empty);
        }

        public void PauseAsync()
        {
            Logger.Debug("PauseAsync - State = {0}", State);

            lock (Sync)
            {
                if (State == EState.Paused)
                    return;

                State = EState.Paused;                
            }

            OnPauseBackup?.Invoke(this, EventArgs.Empty);
        }

        private async Task StartBackup(DirectoryInfo sourcePath, DirectoryInfo targetPath, SecureString secret, bool overwrite = false, bool cacheTargetPathContent = false)
        {
            Logger.Debug("StartBackup {0} {1} {2} {3} {4}", sourcePath, targetPath, State, overwrite, cacheTargetPathContent);

            try
            {
                var sourceFiles = sourcePath.GetFiles("*.*", SearchOption.AllDirectories);

                if (!sourceFiles.Any())
                {
                    Logger.Debug("Nothing to backup! Stopping...");

                    return;
                }

                if (!Directory.Exists(targetPath.FullName))
                {
                    Logger.Debug("Creating output directory " + targetPath.FullName);

                    Directory.CreateDirectory(targetPath.FullName);
                }

                await Task.Run(() =>
                {
                    int current = 1;

                    int total = sourceFiles.Length;

                    FileInfo[] targetPathFiles = null;

                    // If caching is enabled, get all file information from target path to speed up check, if file already exists, in case it should not be overwritten
                    if (cacheTargetPathContent)
                    {
                        OnCacheDestinationPathContent?.Invoke(this, EventArgs.Empty);

                        targetPathFiles = targetPath.GetFiles("*.*", SearchOption.AllDirectories);
                    }

                    foreach (var sourceFile in sourceFiles)
                    {
                        var backupItem = BackupFile(current, sourceFile, sourcePath, targetPath, secret, overwrite, targetPathFiles);

                        OnAfterBackupItem?.Invoke(this, new AfterEventArgs(total, current++, backupItem));

                        if (State == EState.Stopped)
                            return;
                    }
                });
            }
            catch (Exception e)
            {
                Logger.Error(e, "Fail");
            }
            finally
            {
                StopAsync();

                OnDoneBackup?.Invoke(this, new EventArgs());
            }
        }

        private IBackupItem BackupFile(int number, FileInfo sourceFile, DirectoryInfo sourcePath, DirectoryInfo targetPath, SecureString secret, bool overwrite = false, FileInfo[] targetPathFiles = null)
        {
            Logger.Debug("BackupFile {0} {1} {2} {3} {4} {5} {6}", number, sourceFile, sourcePath, targetPath, State, overwrite, targetPathFiles != null);
                                
            IBackupItem backupItem = null;

            try
            {
                while (State == EState.Paused)
                    Thread.Sleep(100);

                if (State == EState.Stopped)
                    return backupItem;

                var current = DateTime.Now;

                backupItem = new BackupItem(number, sourceFile, sourcePath, targetPath);

                OnBeforeBackupItem?.Invoke(this, new BeforeEventArgs(backupItem));

                backupItem.DoBackup(secret, overwrite, targetPathFiles);

                var durationInMilliSeconds = (DateTime.Now - current).Milliseconds;

                Logger.Info("BackupFile\t{0}ms\t{1} {2} {3} {4} {5} {6} {7}", durationInMilliSeconds, number, sourceFile, sourcePath, targetPath, State, overwrite, targetPathFiles != null);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Failed to backup file");
            }

            return backupItem;
        }
    }
}
